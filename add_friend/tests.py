# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend
from .models import Friend
from .forms import Friend_Form

    # Create your tests here.
class AddFriendUnitTest(TestCase):

    def test_add_friend_url_is_exist(self):
        response = Client().get('/add_friend/')
        self.assertEqual(response.status_code, 200)

    def test_addfriend_using_index_func(self):
        found = resolve('/add_friend/')
        self.assertEqual(found.func, index)

    def test_model_can_add_new_friend(self):
        # Creating a new activity
        new_friend = Friend.objects.create(name='Si Buta dari Gua Hantu', url='https://google.com' )

        # Retrieving all available activity
        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    # def test_form_validation_for_blank_items(self):
    #     form = Friend_Form(data={'name': ''})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(
    #         form.errors['name'], ["This field is required."]
    #     )
    # def test_addfriend_post_success_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/add_friend/add_friend', {'name': test})
    #     self.assertEqual(response_post.status_code, 302)

    #     response= Client().get('/add_friend/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn(test, html_response)

    def test_addfriend_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/add_friend/add_friend', {'name': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_addfriend_delete(self):
        new_friend = Friend.objects.create(
            name='Anonymous'
        )
        response_post = Client().get('/add_friend/delete/{}/'.format(new_friend.id))
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.count(), 0)


