from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

author = 'MT , Adil , Elvan'
response = {}

def index(request):
    response['author'] = author
    friend = Friend.objects.all().order_by('-created_date')
    response['friend'] = friend
    html = 'add_friend/add_friend.html'
    response['friend_form'] = Friend_Form
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['url'] = request.POST['url'] if request.POST['url'] != "" else "Anonymous"
        friendlist = Friend(name=response['name'], url=response['url'])
        friendlist.save()
        return HttpResponseRedirect('/add_friend/')
    else:
        return HttpResponseRedirect('/add_friend/')

def delete_friend(request, pk):
    friend = Friend.objects.filter(pk=pk).first()
    if friend != None:
        friend.delete()
        pass
    return HttpResponseRedirect('/add_friend/')
