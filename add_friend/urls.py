from django.conf.urls import url
from .views import index, add_friend, delete_friend


urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_friend', add_friend, name='add_friend'),
	url(r'^delete/(?P<pk>\d+)/$', delete_friend, name='delete_friend')
 ]
