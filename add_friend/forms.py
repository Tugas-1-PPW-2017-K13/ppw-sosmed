from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan url',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(required=True, widget=forms.URLInput(attrs=attrs))
    # message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
