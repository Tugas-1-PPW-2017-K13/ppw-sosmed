from django.db import models
from django.utils import timezone
import pytz

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=27)
    url = models.URLField()
    created_date = models.DateTimeField(auto_now_add=True)