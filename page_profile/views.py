from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import data as Data

# Create your views here.
profile=[{'subject' : 'Birthday','value':Data.birthdate.strftime('%d %B %Y')},\
{'subject' : 'Gender','value':Data.gender},\
{'subject' : 'Expertise','value':Data.expertise},\
{'subject' : 'Description','value':Data.description},\
{'subject' : 'Email','value':Data.email}]
name=Data.name
response={}

def index(request):
	response={'author':'MT , Adil , Elvan','profile':profile,'Name':name}
	response ['birthdate']=Data.birthdate.strftime('%d %B %Y')
	response ['gender']=Data.gender
	response ['expertise']=Data.expertise
	response ['description']=Data.description
	response ['email']=Data.email

#	response ['name']='Muhammad Aulia Adil'
#	response ['birthdate']='5 November'
#	response ['gender']='Male'
#	response ['expertise']=['Marketing','Public Speaking','Videography']
#	response ['description']='Mahasiswa Sistem Informasi 2016 Fasilkom UI'
#	response ['email']='m.aulia.adil@gmail.com'
	html = 'page_profile/page_profile.html'
	return render(request, html, response)
	
