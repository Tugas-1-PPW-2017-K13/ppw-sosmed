from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index , name

# Create your tests here.
class Page_Profile_Test(TestCase):
	def test_page_is_exist(self):
		response = Client().get('/page_profile/')
		self.assertEqual(response.status_code,200)

	def test_page_using_index_func(self):
		found = resolve('/page_profile/')
		self.assertEqual(found.func, index)
		
	def test_css_is_bootstrap(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('bootstrap.min.css', html_response)

	def test_index_contains_subject(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Birthday', html_response)
		self.assertIn('Gender', html_response)
		self.assertIn('Expertise', html_response)
		self.assertIn('Description', html_response)
		self.assertIn('Email', html_response)
		
	def test_landing_page_content_is_written(self):
		self.assertIsNotNone(name)