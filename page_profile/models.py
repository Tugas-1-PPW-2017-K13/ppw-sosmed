from django.db import models
from datetime import date

# Create your models here.
class data(models.Model):
	name = "Muhammad Aulia Adil"
	birthdate=date(1998,11,5)
	expertise=["Marketing","Public Speaking","Videography"]
	description="Mahasiswa Sistem Informasi 2016 Fasilkom UI"
	email = "m.aulia.adil@gmail.com"
	gender = "Male"