# PPW-SOSMED Web Project for Tugas 1

## Team composition:
1. Elvan Rizky Novandi |NPM: 1606822756
2. Michael Tengganus   |NPM: 1606917651
3. Muhammad Aulia Adil |NPM: 1606874671

## Pipelines status
[![pipeline status](https://gitlab.com/Tugas-1-PPW-2017-K13/ppw-sosmed/badges/master/pipeline.svg)](https://gitlab.com/Tugas-1-PPW-2017-K13/ppw-sosmed/commits/master)

## code coverage status
[![coverage report](https://gitlab.com/Tugas-1-PPW-2017-K13/ppw-sosmed/badges/master/coverage.svg)](https://gitlab.com/Tugas-1-PPW-2017-K13/ppw-sosmed/commits/master)

## heroku link
	ppwsosmed.herokuapp.com
