from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

class page_statsUnitTest(TestCase):
	
	def test_lab2_using_index_func(self):
		found = resolve('/page_stats/')
		self.assertEqual(found.func, index)
