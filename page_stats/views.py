from django.shortcuts import render
from django.http import HttpResponseRedirect
from update_status.models import Status
from add_friend.models import Friend

# Create your views here.
response = {}
def index(request):
	response['author'] = "MT, Adil, Elvan"
	response ['name']='Muhammad Aulia Adil'
	response ['Friends']=len(Friend.objects.all().order_by('-created_date'))
	response['feed']=len(Status.objects.all().order_by('-created_date'))
	if (len(Status.objects.all().order_by('-created_date'))!=0):
		response ['database']=Status.objects.all().order_by('-created_date')[0]
	else:
		pass
	html = 'Stats/Page_stats.html'
	return render(request, html, response)
	
